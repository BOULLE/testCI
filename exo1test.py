import unittest
from exo1 import carre

class TestSquareFunction(unittest.TestCase):
    def testcarrenegatif(self):
        self.assertEqual(carre(3), 9)
        self.assertEqual(carre(-2), 4)

            
if __name__ == '__main__':
    unittest.main()
